For using as a script:
```sh
> peparser path_to_exe
```

For using as a python module:
```sh
>>> import peparser
>>> help(peparser)
```
