'''
PE parser.
It's based on https://habrahabr.ru/post/266831/
Example of PE structure: https://goo.gl/rpfgc4
'''

from datetime import datetime as dt
import struct
import sys
import os.path

DIRECTORY_EXPORT          = 0  # Export Directory
DIRECTORY_IMPORT          = 1  # Import Directory
DIRECTORY_RESOURCE        = 2  # Resource Directory
DIRECTORY_EXCEPTION       = 3  # Exception Directory
DIRECTORY_SECURITY        = 4  # Security Directory
DIRECTORY_BASERELOC       = 5  # Base Relocation Table
DIRECTORY_DEBUG           = 6  # Debug Directory
DIRECTORY_ARCHITECTURE    = 7  # Architecture Specific Data
DIRECTORY_GLOBALPTR       = 8  # RVA of GP
DIRECTORY_TLS             = 9  # TLS Directory
DIRECTORY_LOAD_CONFIG     = 10 # Load Configuration Directory
DIRECTORY_BOUND_IMPORT    = 11 # Bound Import Directory in headers
DIRECTORY_IAT             = 12 # Import Address Table
DIRECTORY_DELAY_IMPORT    = 13 # Delay Load Import Descriptors
DIRECTORY_COM_DESCRIPTOR  = 14 # COM Runtime descriptor

_IMAGE_NUMBEROF_DIRECTORY_ENTRIES = 16
_PE32_MAGIC = '0b01'
_PE64_MAGIC = '0b02'
_ROM_MAGIC = '0701'

class _ParsingResult:
    pass

def _align_up(x, align):
	return (x & ~(align - 1)) + align if (x & (align - 1)) else x

def _def_section(rva, sections, saligment):
    for i in range(len(sections)):
        start = int(sections[i]['vaddr'], 16);
        end = start + _align_up(int(sections[i]['vsize'], 16), saligment);
        if rva >= start and rva < end:
            return i;
    return -1;

def section_alignment():
    '''Returns a value stored after parse() execution. 
    '''
    if _g_section_alignment is None:
        print('Section aligment is not set. Call parse() first.')
        return 0
    else:
        return _g_section_alignment

def rvaToOff(rva, sections, section_aligment):
    '''Function for converting RVA to RAW.

    RAW - offset from the beginning of a file.
    RAW = RVA - sectionRVA + sectionRAW
    sectionRAW - offset to a section from a file start.
    sectionRVA is stored inside a section.

    Example of usage:
    rva = int(pe.dirs[1]['vaddr'], 16)
    rvaToOff(rva, pe.sections, pe.section_alignment())
    Where pe is a result of parse() execution.
    '''
    indexSection = _def_section(rva, sections, section_aligment);
    if(indexSection != -1):
        sectionRVA = int(sections[indexSection]['vaddr'],16)
        sectionRAW = int(sections[indexSection]['raddr'],16)
        return rva - sectionRVA + sectionRAW
    else:
        return 0;

def parse(path):
    '''Simple parsing of PE file.

    Prints basic info and returns an object with 2 fields: sections, dirs.
    sections, dirs - lists of dictionaries.
    '''
    f=open(path, 'rb')

    dos_header = f.read(0x40)
    _pe_start_hex = hex(struct.unpack('<I', dos_header[-4:])[0])
    print('Start of PE header: {0}'.format(_pe_start_hex))

    f.seek(int(_pe_start_hex, 16))
    signature = f.read(4)

    ## IMAGE_FILE_HEADER
    file_header = f.read(20)
    _sections_count = struct.unpack("<h", file_header[2:4])[0]
    _timestamp = struct.unpack("<I", file_header[4:8])[0]
    print('Sections count: {0}'.format(_sections_count))
    print('Created: {0}'.format(dt.fromtimestamp(_timestamp).strftime('%c')))

    ## IMAGE_OPTIONAL_HEADER
    data_directory_size = _IMAGE_NUMBEROF_DIRECTORY_ENTRIES * (4 + 4)
    optional_header = f.read(96 + data_directory_size)

    data_directories=[]
    for i in range(_IMAGE_NUMBEROF_DIRECTORY_ENTRIES):
        pos = 96 + i*8
        data = optional_header[pos : pos + 8]
        data_directories.append({
            'vaddr':hex(struct.unpack('<I', data[:4])[0]),
            'size' :hex(struct.unpack('<I', data[-4:])[0]),
            })

    _magic_hex = optional_header[0:2].hex()
    if _magic_hex == _PE32_MAGIC:
        file_type = 'PE32'
    elif _magic_hex == _PE64_MAGIC:
        file_type = 'PE64'
    elif _magic_hex == _ROM_MAGIC:
        file_type = 'ROM image'
    else:
        file_type = 'Unknown'
    print('File type: {0}'.format(file_type))
    _image_base_hex = hex(struct.unpack('<I', optional_header[28:32])[0])
    global _g_section_alignment
    _g_section_alignment = struct.unpack('<I', optional_header[32:36])[0]
    _section_alignment_hex = hex(_g_section_alignment)
    print('Image base: {0}'.format(_image_base_hex))
    print('Section aligment : {0}'.format(_section_alignment_hex))
    print('End of PE-Header: {0}'.format(hex(f.tell() - 1)))

    ## SECTIONS
    sections=[]
    for i in range(_sections_count):
        data = f.read(0x28)
        sections.append({
            'name':data[:8],
            'vsize':hex(struct.unpack('<I', data[8:12])[0]),
            'vaddr':hex(struct.unpack('<I', data[12:16])[0]),
            'rsize':hex(struct.unpack('<I', data[16:20])[0]),
            'raddr':hex(struct.unpack('<I', data[20:24])[0]),
            })
    res = _ParsingResult
    res.sections = sections
    res.dirs = data_directories
    return res

def print_sections(sections):
    print("Sections:")
    for s in sections:
        print(s['name'].decode("utf-8"))
        print("      Virtual size:    " + s['vsize'])
        print("      Virtual address: " + s['vaddr'])
        print("      Raw size:        " + s['rsize'])
        print("      Raw address:     " + s['raddr'])
    

def main(argv):
    if not len(argv) == 2 or not os.path.exists(argv[1]):
        print("You must to specify path to an executable file!")
        exit()
    print_sections(parse(argv[1]).sections)
        

if __name__ == "__main__":
    main(sys.argv)
